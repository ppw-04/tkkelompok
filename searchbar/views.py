from django.shortcuts import render, redirect, get_object_or_404
from django.http import *
from django.http import HttpResponseRedirect
from .forms import Spaces
from .models import Spacesmodels

# Create your views here.


def searchpage(request):
	return render(request, 'search.html')

def lihatspaces(request):
    all= Spacesmodels.objects.all()

    arg= {
        "previous": all
    }
    return render(request, "spaces.html",arg)

def add(request):
    val1 = request.GET['harga']
    val2 = request.GET['jam']
    res = val1 * val2
    return render(request,"price.html",{"result" : res})
    
def rent(request,id):
    rentee = get_object_or_404(Spacesmodels, pk = id)
    form = Spaces()
    arg = {
        "rentee":rentee,
        "form":form
    }
    if request.method == "POST":
        hours = request.POST['jam']
        rate = rentee.harga
        return pricing(request,rate,hours)
    else:
        return render(request,'rent.html',arg)

def pricing(request,rate,hours):
    price = int(rate) * int(hours)
    arguement = {
        "harga" : price
    }
    return render(request,"pricing.html",arguement)
from django import forms

class Spaces(forms.Form):
    nama = forms.CharField(
        label = "Space Name", 
        max_length = 50
    )

    isiankota=(
        ('Depok','Depok'),
        ('Jakarta','Jakarta'),
        ('Bandung','Bandung'),
        ('Bogor','Bogor'),
    )
    kota =forms.ChoiceField(
        label =  "City",
        choices = isiankota, 
    )

    isiantipe=(
        ('Event Space','Event Space'),
        ('Meeting Room','Meeting Room'),
        ('Creative Studio','Creative Studio'),
        ('Working Space','Working Space'),
    )

    tipe = forms.ChoiceField(
        label = "Space Type", 
        choices = isiantipe, 
    )

    harga = forms.IntegerField(
        label = "Rate", 
    )
    jam = forms.IntegerField(
        label= "Hours"
    )

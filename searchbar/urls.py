from django.contrib import admin
from django.urls import path,include
from . import views

urlpatterns = [
    path('searchpage/', views.searchpage, name="searchpage"),
    path('spaces/', views.lihatspaces, name ="lihat_spaces"),
    path('rent/<int:id>',views.rent,name = "rent"),
    path('process/',views.pricing)
]

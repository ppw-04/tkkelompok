from django.test import TestCase
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from importlib import import_module
from django.urls import resolve, reverse
from django.conf import settings

from django.test import TestCase
from django.test import TestCase,Client,LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
import time


from .views import *
# Create your tests here.

class rentfeature(TestCase):

    # test 1: searchpage.html
    def test_ada_url_searchpage(self):
        response = Client().get('/searchpage/')
        self.assertEqual(response.status_code, 200)

    def test_html_searchpage(self):
        response = Client().get('/searchpage/')
        self.assertTemplateUsed(response, 'search.html')
    
    def test_ada_accordion(self):
        c = Client()
        response = c.get('/searchpage/')
        content = response.content.decode('utf8')
        self.assertIn ("accordion", content)

    def test_konten_ada_daftar_lokasi(self):
        c = Client()
        response = c.get('/searchpage/')
        content = response.content.decode('utf8')
        self.assertIn ("Location", content)
    
    def test_konten_ada_daftar_type(self):
        c = Client()
        response = c.get('/searchpage/')
        content = response.content.decode('utf8')
        self.assertIn ("Type", content)

    def test_ada_toggle(self):
        c = Client()
        response = c.get('/searchpage/')
        content = response.content.decode('utf8')
        self.assertIn ("toggle", content)

    #test 2: spaces.html
    def test_ada_url_spaces(self):
        response = Client().get('/spaces/')
        self.assertEqual(response.status_code, 200)

    def test_html_spaces(self):
        response = Client().get('/spaces/')
        self.assertTemplateUsed(response, 'spaces.html')

    def test_ada_form(self): 
        response = Client().get('/spaces/')
        content = response.content.decode('utf8')
        self.assertIn ("<form", content)
    
    # test 3: rent.html
    def test_ada_url_rent(self):
        url = reverse('rent', args=[1])
        self.assertEqual(url, '/rent/1')

    # def test_html_rent(self):
    #     response = Client().get('rent/<int:id>')
    #     content = response.content.decode('utf8')
    #     self.assertTemplateUsed(response, 'rent.html')

    # def test_button_submit(self):
    #     c = Client()
    #     response = c.get('/rent/1')
    #     content = response.content.decode('utf8') 
    #     self.assertIn ("<button", content) 
    #     self.assertIn("Calculate", content)

    # test 4: pricing.html
    def test_ada_url_pricing(self):
        url = reverse('rent', args=[1])
        self.assertEqual(url, '/rent/1')

    # def test_html_pricing(self):
    #     response = Client().get('/rent/1')
    #     self.assertTemplateUsed(response, 'pricing.html')

class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_dark_mode_light_mode(self):
        # selenium = self.selenium
        self.browser.get('http://127.0.0.1:8000/searchpage')

        time.sleep(5)
        e = self.browser.find_element_by_css_selector(".toggle")
        time.sleep(2)
        e.click()
        time.sleep(2)

        light = "rgba(255, 255, 255, 1)"
        dark = "rgba(1, 1, 1, 1)"

        cbl = "rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box"
        cbn = "rgb(42, 42, 42) none repeat scroll 0% 0% / auto padding-box border-box"

        chl= "rgb(47, 75, 110) none repeat scroll 0% 0% / auto padding-box border-box"
        chn= "rgb(47, 75, 110) none repeat scroll 0% 0% / auto padding-box border-box"

        lh1= "rgba(0, 0, 0, 1)"
        nh1= "rgba(255, 255, 255, 1)"


        bg = self.browser.find_element_by_tag_name("body").value_of_css_property("background-color")
        self.assertEqual(bg, dark)

        h1 = self.browser.find_element_by_tag_name("h1").value_of_css_property("color")
        self.assertEqual(nh1, h1)

        cb = self.browser.find_element_by_class_name("card-body").value_of_css_property("background")
        self.assertEqual(cb, cbn)

        ch = self.browser.find_element_by_class_name("card-header").value_of_css_property("background")
        self.assertEqual(chn, ch)

        e.click()
        time.sleep(2)

        bg = self.browser.find_element_by_tag_name("body").value_of_css_property("background-color")
        self.assertEqual(bg, light)

        h1 = self.browser.find_element_by_tag_name("h1").value_of_css_property("color")
        self.assertEqual(lh1, h1)

        cb = self.browser.find_element_by_class_name("card-body").value_of_css_property("background")
        self.assertEqual(cbl, cb)

        ch = self.browser.find_element_by_class_name("card-header").value_of_css_property("background")
        self.assertEqual(chl, ch)

        time.sleep(2)

    def test_klik_accordion(self):
        self.browser.get('http://127.0.0.1:8000/searchpage')
        
        # find the element
        acc1 = self.browser.find_element_by_id('headingOne')
        acc2 = self.browser.find_element_by_id('headingTwo')

        # test
        time.sleep(1)
        acc2.click()
        time.sleep(1)
        acc1.click()
        time.sleep(1)
    
    def test_klik_here(self):
        self.browser.get('http://127.0.0.1:8000/searchpage')

        # find the element
        buttonhere = self.browser.find_element_by_id('btnhere')

        #test di searchpage
        time.sleep(2)
        buttonhere.click()
        time.sleep(2)

        self.assertEqual(self.browser.current_url, "http://127.0.0.1:8000/spaces/")

    #test di spaces (udah bisa di local tp pas deploy gabisa terus :( )
    # def test_klik_rent(self):
    #     self.browser.get('http://127.0.0.1:8000/spaces')
    #     buttonrentnow = self.browser.find_element_by_id('btnspc')
    #     time.sleep(2)
    #     buttonrentnow.click()
    #     time.sleep(2)
    #     self.assertEqual(self.browser.current_url, "http://127.0.0.1:8000/rent/1")


    #YG INI BLM BISA 
        #test di rent 
        # inputjam = self.browser.find_element_by_id('renthours')
        # time.sleep(2)
        # inputjam.send_keys("4")
        # time.sleep(2)
        # calculatebutton = self.browser.find_element_by_id("button")
        # calculatebutton.click()

        # self.assertEqual(self.browser.current_url, "http://127.0.0.1:8000/rent/1")






    


 


from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve,reverse
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time
import unittest
# Create your tests here.
class SignupTests(TestCase):
    #URLS AND VIEWS
    urls = 'signup.urls'
    def test_url_isexist(self):
        response = Client().get('/signup/')
        self.assertEquals(response.status_code,200)
    def test_sign_url_resolve(self):
        function = resolve('/signup/')
        self.assertEquals(function.func,signup)
    def test_tabs_exist(self):
        response = Client().get('/signup/')
        content = response.content.decode('utf8')
        self.assertIn ("tabs", content)
    def test_redirect(self):
        response = Client().get('/login')
        self.assertEquals(response.status_code,301)




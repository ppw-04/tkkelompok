from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout


def homepage(request):
    context = {
        'judul' : 'homepage.'
    }

    template = None
    
    if request.user.is_authenticated: 
        template = 'homepage_logout.html'
    else:
        template = 'homepage.html'

    return render(request, template, context)

def loginView(request):
    context = {
        'judul' : 'Please log in to your account first to book your space.',
    }

    user = None
    if request.method == "GET":     
        if request.user.is_authenticated:
            return redirect('/spaces/')

    
    if request.method == "POST": # pragma: no cover
        username_login = request.POST['username']
        password_login = request.POST['password']

        user = authenticate(request, username=username_login, password=password_login)
        
        if user is not None:
            login(request, user) 
            return redirect('/spaces/')
        else:
            return redirect('/login/')
            
    return render(request, 'login.html', context)



@login_required
def logoutView(request): # pragma: no cover
    context = {
        'judul' : "Do you want to Logout?"
    }

    if request.method == "POST":
        if request.POST["logout"] == "Submit":
            logout(request)
        
        return redirect('homepage')

    
    return render(request, 'logout.html', context)
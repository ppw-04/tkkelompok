from django.test import TestCase, Client
from django.urls import reverse, resolve

from .views import loginView, homepage, logoutView
from django.http.request import HttpRequest


# Create your tests here.

class Login_Test(TestCase):
    def test_login_url(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_logout_url(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_jika_url_tidak_ada(self):
        response = Client().get('/url-tidak-ada')
        self.assertEqual(response.status_code, 404)

    def test_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')

    def test_login_function(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginView)

    def test_logout_function(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logoutView)

    def test_homepage_function(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def test_judul_login(self): # pragma: no cover
        request = HttpRequest()
        response = loginView(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Please log in to your account first to book your space.', html_response)
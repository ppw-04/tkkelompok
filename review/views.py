from django.shortcuts import render
from .models import ReviewModels
from django.http import HttpResponseRedirect

# Create your views here.
def review(request):
    create_review={
        'listReview' : ReviewModels.objects.all(),
    }
    return render(request, 'review.html', create_review)

def form(request):
    if request.method == 'POST' :
        name = request.POST['name']
        email = request.POST['email']
        review = request.POST['review']

        ReviewModels.objects.create(
            name = name,
            email = email,
            review = review
        ) 
        return HttpResponseRedirect("/review/")

def delete(request, delete_id):
    ReviewModels.objects.filter(id = delete_id).delete()
    return HttpResponseRedirect("/review/")

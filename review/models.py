from django.db import models

# Create your models here.
class ReviewModels(models.Model) :
    name = models.CharField(max_length = 30)
    email = models.EmailField(max_length = 30)
    review = models.TextField()

    def __str__(self):
        return self.name


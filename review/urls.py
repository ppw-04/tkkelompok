from django.urls import path, re_path
from . import views

app_name = 'review'
urlpatterns = [
    path('review/', views.review, name='review_urls'),
    path('review/make_review', views.form),
    re_path('delete/(?P<delete_id>.*)/', views.delete, name='delete'),
]
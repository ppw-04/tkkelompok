from django.test import TestCase, Client
from django.urls import resolve

from .views import review
from .views import form
from .models import ReviewModels

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# Create your tests here.

class unitTest(TestCase):
    def test_url_bisa_diakses (self):
        response = Client().get('/review/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_panggil_review_dari_views (self):
        response = resolve('/review/')
        self.assertEqual(response.func, review)
    
    def test_views_panggil_html_review (self):
        response = Client().get('/review/')
        self.assertTemplateUsed(response, 'review.html')

    def test_html_ada_forms (self):
        response = Client().get('/review/')
        content = response.content.decode('utf8')
        self.assertIn('<form ', content)
    
    def test_ada_modelnya (self, nama='yunie', email='yunie@gmail.com', review='awesome place!' ):
        return ReviewModels.objects.create(name = nama, email=email, review=review)

    def test_model_menyimpan_masukan(self):
        review = ReviewModels.objects.create(name = 'coba model', email='cobamodel@gmail.com', review='hanya coba')
        hitung_isi_model = ReviewModels.objects.all().count()
        self.assertEqual(hitung_isi_model, 1)

    def test_key_modelnya(self):
        name = ReviewModels.objects.create(name = 'yunie')
        self.assertEqual('yunie', name.__str__())

    def test_ada_tombol_submit(self):
        response = Client().get('/review/')
        content = response.content.decode('utf8')
        self.assertIn('Submit', content)



# class functionalTest(TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')        
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium  = webdriver.Chrome(chrome_options=chrome_options)
#         super(functionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(functionalTest, self).tearDown()

#     def test_input_review(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('http://127.0.0.1:8000/review')
#         # find the form element
#         name = selenium.find_element_by_id('id_name')
#         email = selenium.find_element_by_id('id_email')
#         review = selenium.find_element_by_id('id_review')
#         name.send_keys('Yunie Debora')
#         email.send_keys('yunie@gmail.com')
#         review.send_keys('Yok bisa yok!')

#         submit = selenium.find_element_by_name('submit')
#         submit.send_keys(Keys.RETURN)
#         self.assertIn('Yunie Debora', selenium.page_source)
#         self.assertIn('yunie@gmail.com', selenium.page_source)
#         self.assertIn('Yok bisa yok!', selenium.page_source)
    
#     def test_delete_review(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('http://127.0.0.1:8000/review')
#         # find the form element
#         name = selenium.find_element_by_id('id_name')
#         email = selenium.find_element_by_id('id_email')
#         review = selenium.find_element_by_id('id_review')
#         name.send_keys('Yunie Debora')
#         email.send_keys('yunie@gmail.com')
#         review.send_keys('Yok bisa yok!')

#         submit = selenium.find_element_by_name('submit')
#         submit.send_keys(Keys.RETURN)

#         delete = selenium.find_element_by_id('delete')
#         delete.send_keys(Keys.RETURN)
#         jumlah = ReviewModels.objects.all().count()
#         self.assertEqual(jumlah, 0)